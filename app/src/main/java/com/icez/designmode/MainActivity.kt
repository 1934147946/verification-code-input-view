package com.icez.designmode

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.icez.verification_code_input_view.VerificationCodeInputView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val vciv = findViewById<VerificationCodeInputView>(R.id.vciv)
//        vciv.setOnClickListener {
//            openSoftKeyboard(vciv)
//        }

    }

    /**
     *  打开软键盘
     * @param view
     */
    public fun openSoftKeyboard(view: View?) {

        view?.isFocusableInTouchMode = true
        view?.setFocusable(true)
        view?.requestFocus()
        val imm =
            view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

    }
}